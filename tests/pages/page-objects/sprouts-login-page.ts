import { click, fill, gotoURL } from 'utils/action-utils';
import { expectElementToHaveText, expectPageToHaveURL } from 'utils/assert-utils';
import { testdata } from '../../testdata/qa-env-test-data';
import { expect } from '@playwright/test';
import { getAttribute } from 'utils/element-utils';
import { getLocator } from 'utils/locator-utils';

const username ='#emailId';
const password='#userPassword';
const signInButton = () => getLocator(`button#login-submit-button`);
const cleintName = '//*[contains(@value,"Sprouts Test")]';
const avatorIcon = '//*[contains(@class,"MuiAvatar-root")]';
const manageProfile ='//*[@class="profile-menu-item"]';
const manageProfileEmail = '//*[contains(@value,"@")]';
const errorMessageBlankEmail='#emailId-helper-text';
const errorMessage='//*[@class="login-error-text"]';



export async function navigatetoSproutsLoginPage() {
  await gotoURL(testdata.sproutsURL);
}


export async function verifySproutsLoginPageURL() {
  await expectPageToHaveURL(new RegExp('https://qa.sprouts.ai/login'));
}

export async function enterCredentials(email:string,emailpwd:string) {
  await fill(username, email);
  await fill(password, emailpwd);
}

export async function clickSignIn() {
  await click(signInButton());
}

export async function verifyLogin(clientName:string) {
  const attributeValue = await getAttribute(cleintName ,'value');
  await expect(attributeValue).toBe(clientName);
}

export async function verifyUserEmail(profileEmail:string) {
  await click(avatorIcon);
  await click(manageProfile);
  const attributeValue = await getAttribute(manageProfileEmail ,'value');
  await expect(attributeValue).toBe(profileEmail);
}

export async function verifyCredentialsError(credentialType:string) {
  switch (credentialType) {
    case "Blankemail":
     await expectElementToHaveText(errorMessageBlankEmail,'Please provide a valid Email address.')
      break;
    case "incorrectCredentials":
      await expectElementToHaveText(errorMessage,'The email or password you entered is incorrect. Please check and try again.')
      break;
    default:
      console.log("Unknown credential Type.");
}
}