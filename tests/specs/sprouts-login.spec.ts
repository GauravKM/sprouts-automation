import { test } from 'setup/page-setup';
import { testdata } from '../testdata/qa-env-test-data';
import {navigatetoSproutsLoginPage, verifySproutsLoginPageURL, enterCredentials, verifyLogin, clickSignIn,verifyUserEmail, verifyCredentialsError} from '../pages/page-objects/sprouts-login-page';
test.describe.configure({  });
test.describe('test login feature', () => {
  test('valid login credentials scenario', async () => {
    await navigatetoSproutsLoginPage();
    await verifySproutsLoginPageURL(); 
    await enterCredentials(testdata.username,testdata.password);
    await clickSignIn();
    await verifyLogin(testdata.clientName);
    await verifyUserEmail(testdata.username);
   
    
  });

  test('valid blank credentials scenario', async () => {
    await navigatetoSproutsLoginPage();
    await verifySproutsLoginPageURL();
    await clickSignIn();
    await verifyCredentialsError('Blankemail')
  });

  test('valid invalid credentials scenario', async () => {
    await navigatetoSproutsLoginPage();
    await verifySproutsLoginPageURL();
    await enterCredentials('test@test.com','Test@123');
    await clickSignIn();
    await verifyCredentialsError('incorrectCredentials')
  });


});


